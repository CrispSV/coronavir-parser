# CoronaVirus COVID-19 Live Tracking


# Display on OLED latest Corona virus (COVID-19) data for selected country. Data update on constant basis. Hardware is NODEMCU based.

This code is based on Volkan's (https://engineeringvolkan.wordpress.com/) initial commit on github.

Updated code (v2) to display current time. Also instead of recovered cases, new code display Critical as I think it is more important info.

 1- Change your wifi information from **WifiConnect.h** file

  `char ssid[32] = "yourssid";`
  `char password[64] = "yourpass";`

 2- Define your country code to **corona.ino** file
 
    #define country_code "yourcountrycode"  (eg Greece)
   
   GET (https://coronavirus-19-api.herokuapp.com/countries) -> all countries info
 
 3- Define your timezone and daylight setting (for time to display correctly for your region) to **corona.ino** file
 
   `int timezone = 2;  //Timezone 2 is for Greece`
   
   `int dst = 0;       //daylight saving time`
   
 