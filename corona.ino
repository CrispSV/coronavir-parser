// Corona COVID-19 virus incidents parser
// DrmacGR (SV1NJX) v2 - March 2020

#include <ESP8266HTTPClient.h> 
#include "json_parser.h"
#include "WifiConnect.h"
#include "SSD1306Wire.h"
#include "time.h"
#include "Wire.h"

#define s2ms(second) (second*1000)
unsigned long long prev_millis(0);

#define country_code "Greece"

int timezone = 2;   //Timezone (2 for Greece)
int dst = 0;        //Daylight saving time


// OLED Display Settings
const int I2C_DISPLAY_ADDRESS = 0x3c;
#if defined(ESP8266)
const int SDA_PIN = D3;
const int SDC_PIN = D4;
#else
const int SDA_PIN = 5; //D3;
const int SDC_PIN = 4; //D4;
#endif

int interval = s2ms(60);
unsigned long long PreviousMillis = 0;
unsigned long long CurrentMillis = interval;
bool bFirstKickMillis = false;

extern bool bGotIpFlag;

static String build_url_from_country(String country)
{
  String url = "http://coronavirus-19-api.herokuapp.com/countries/";
  url = url + country;
  return url;
}

// Initialize the oled display for address 0x3c
// sda-pin=14 and sdc-pin=12
 SSD1306Wire     display(I2C_DISPLAY_ADDRESS, SDA_PIN, SDC_PIN);

 
void setup(void)
{ 

// initialize display
  display.init();
  display.clear();
  display.display();
  
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setContrast(255);
 
// Show application info  
    display.clear();
    display.drawString(60, 0, "CORONA VIRUS DATA");
    display.drawString(60, 10, "v2   DrmacGR"  );
    display.display();
 delay (3000);

// TIME Read from NTP server
 configTime(timezone * 3600, dst * 0, "pool.ntp.org", "time.nist.gov"); 
 Serial.println("\nWaiting for time");
  while (!time(nullptr)) {
    Serial.print(".");
    delay(1000);
  }
 
 
  #if defined JSON_DEBUG
    Serial.begin(9600);
  #endif
  
  JSON_LOG("Connecting...");
  display.clear();
    display.drawString(60, 0, "Connecting...");
    display.drawString(60, 30, "Latest data for " + String(country_code));
    display.display();
    delay(3000);
  vConnWifiNetworkViaSdk();
}

void loop()
{
 
  
  if(bGotIpFlag) bGotIp();
  
  if(bFirstKickMillis) CurrentMillis = millis();
  
  if (!bGotIpFlag && CurrentMillis - PreviousMillis >= interval) 
  {
    if(!bFirstKickMillis) CurrentMillis = 0;
    
    bFirstKickMillis = true;
    
    PreviousMillis = CurrentMillis;
    
    HTTPClient http; 
    http.begin(build_url_from_country(country_code));
    
    int httpCode = http.GET(); 
  
    if(httpCode > 0) 
    {
      String payload = http.getString();
       
      char* JsonArray = (char *)malloc(payload.length() + 1);
      if (!JsonArray) JSON_LOG("nop");
      
      payload.toCharArray(JsonArray, payload.length() + 1);
      
      JSON_LOG(JsonArray);
     
   time_t now = time(nullptr);  //Get Local time
   
      if (json_validate(JsonArray))
      {
        int confirmed = (int)get_json_value(JsonArray, "cases", INT);
        int deaths = (int)get_json_value(JsonArray, "deaths", INT);
        int recovered = (int)get_json_value(JsonArray, "recovered", INT);
        int critical = (int)get_json_value(JsonArray, "critical", INT);
      
        JSON_LOG(confirmed);
        JSON_LOG(deaths);
        JSON_LOG(recovered);
        JSON_LOG(critical);
        
//Display latest Data 
//-------------------       
    
    display.clear();
    display.drawString(30, 0, "Confirmed :");
    display.drawString(80, 0, String(confirmed));
    display.drawString(30, 20, "Deaths ");
    display.drawString(80, 20, String(deaths));
    display.drawString(30, 30, "Critical ");
    display.drawString(80, 30, String(critical));
    display.drawString(65, 50, String(ctime(&now)));
    
    display.display();
  
    
      }
      
      free(JsonArray);
    }
    
    http.end(); 
  }
}